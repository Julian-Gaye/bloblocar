<?php 
require_once "voyage.class.php";

class voyageTable {

    public static function getVoyageByTrajet($trajet) {
        $em = dbconnection::getInstance()->getEntityManager();

        $voyageRepository = $em->getRepository('voyage');
        $voyage = $voyageRepository->findBy(array('trajet' => $trajet->id));

        return $voyage;
    }
	
	public static function getCorrespondancesByTrajet($trajet) {
		$em = dbconnection::getInstance()->getEntityManager()->getConnection();
		
		$query = $em->prepare('select * from find_voyage(\''.$trajet->depart.'\', \''.$trajet->arrivee.'\')');
		$bool = $query->execute();
		if ($bool == false) {
			return NULL;
		}
		return $query->fetchAll();
	}
	
	public static function getVoyageById($id) {
		$em = dbconnection::getInstance()->getEntityManager();

		$voyageRepository = $em->getRepository('voyage');
		$voyage = $voyageRepository->findOneBy(array('id' => $id));	
		
		return $voyage;
	}
	
	public static function ajouterVoyage($trajet, $conducteur, $tarif, $nbPlaces, $heureDepart, $contraintes) {
		$em = dbconnection::getInstance()->getEntityManager();

        $voyageRepository = $em->getRepository('voyage');
		
        $voyage = new voyage(); //On créer un nouvel objet voyage
		$voyage->trajet = $trajet; //On ajoute chaque information dans l'objet voyage (attributs de l'objet)
		$voyage->conducteur = $conducteur;
		$voyage->tarif = $tarif;
		$voyage->nbplace = $nbPlaces;
		$voyage->heuredepart = $heureDepart;
		$voyage->contraintes = $contraintes;
		$em->persist($voyage); //On prépare la nouvelle ligne pour l'insérer dans la bdd
        $em->flush(); //On insère la ligne dans la bdd
	}
}
?>