<?php 
require_once "reservation.class.php";

class reservationTable {
	
	public static function getReservationByVoyage($voyage) {
		$em = dbconnection::getInstance()->getEntityManager();

        $reservationRepository = $em->getRepository('reservation');
        $reservation = $reservationRepository->findBy(array('voyage' => $voyage->id));

        return $reservation;
	}
	
	public static function getIfReservationExistsByVoyageAndUser($voyage, $user) {
		$em = dbconnection::getInstance()->getEntityManager();

        $reservationRepository = $em->getRepository('reservation');
        $reservation = $reservationRepository->findBy(array('voyage' => $voyage->id, 'voyageur' => $user->id));

        if($reservation == false) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public static function reserverVoyage($voyage, $user) {
		$em = dbconnection::getInstance()->getEntityManager();

        $reservationRepository = $em->getRepository('reservation');
		
        $reservation = new reservation();
		$reservation->voyage = $voyage;
		$reservation->voyageur = $user;
		$em->persist($reservation);
        $em->flush();
	}
	
	public static function getReservationByUser($user) {
		$em = dbconnection::getInstance()->getEntityManager();

        $reservationRepository = $em->getRepository('reservation');
        $reservation = $reservationRepository->findBy(array('voyageur' => $user));
		if($reservation == false) {
			return false;
		}
        return $reservation;
	}
	
	public static function annulerReservation($voyage, $user) {
		$em = dbconnection::getInstance()->getEntityManager();

		$reservationRepository = $em->getRepository('reservation');

		$reservation = $reservationRepository->findOneBy(array('voyage' => $voyage->id, 'voyageur' => $user->id));
		
		if($reservation == false) {
			return false;
		}

		$em->remove($reservation);
		$em->flush();
	}
}
?>
