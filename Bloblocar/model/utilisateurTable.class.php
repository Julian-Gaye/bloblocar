<?php
// Inclusion de la classe utilisateur
require_once "utilisateur.class.php";

class utilisateurTable {

  public static function getUserByLoginAndPass($login, $pass)
		{
		$em = dbconnection::getInstance()->getEntityManager();

		$userRepository = $em->getRepository('utilisateur');
		$user = $userRepository->findOneBy(array('identifiant' => $login, 'pass' => sha1($pass)));	
		
		if($user == false) {
			return false;
		}
		return $user->id; 
	}
  
   public static function getUserById($id) {
		$em = dbconnection::getInstance()->getEntityManager();

		$userRepository = $em->getRepository('utilisateur');
		$user = $userRepository->findOneBy(array('id' => $id));	
		
		return $user; 
	}
	
	public static function pseudoIsUsed($pseudo) {
		$em = dbconnection::getInstance()->getEntityManager();

		$userRepository = $em->getRepository('utilisateur');
		$user = $userRepository->findOneBy(array('identifiant' => $pseudo));

		if($user == false) {
			return false;
		}
		else {
			return true;
		}
	}
	
	public static function insertUser($identifiant, $pass, $nom, $prenom) {
		$em = dbconnection::getInstance()->getEntityManager();

		$userRepository = $em->getRepository('utilisateur');
		
		$user = new utilisateur();
		$user->identifiant = $identifiant;
		$user->pass = sha1($pass);
		$user->nom = $nom;
		$user->prenom = $prenom;
		$em->persist($user);
        $em->flush();
	}
}
?>
