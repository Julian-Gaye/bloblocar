<?php 
require_once "trajet.class.php";

class trajetTable {
	
	public static function getTrajet($depart, $arrivee) {
		$em = dbconnection::getInstance()->getEntityManager();
		
		$trajetRepository = $em->getRepository('trajet');
		$trajet = $trajetRepository->findOneBy(array('depart' => $depart, 'arrivee' => $arrivee));
		
		return $trajet;
	}
	
	public static function voyageExists($ville) {
		$em = dbconnection::getInstance()->getEntityManager();

		$userRepository = $em->getRepository('trajet');
		$trajet = $userRepository->findOneBy(array('depart' => $ville)); //On peut aussi vérifeir avec les villes d'arrivée
		
		if($trajet == false) {
			return false;
		}
		else {
			return true;
		}
	}
}
?>
