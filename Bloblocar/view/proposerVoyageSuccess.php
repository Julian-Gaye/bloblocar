<div class="form">	
	<form id="proposerVoyageForm" method="POST">
		<h1>Proposer un voyage</h1>
		<hr>

		<label for="depart"><b>Départ</b></label>
		<input type="text" placeholder="Départ" name="depart" id="depart" onfocus="hideDepartValidite()" onblur="verificationDepart()" required></input>
		<p id="departValidite" class="rouge" for="depart" hidden></p>
		
		<label for="arrivee"><b>Arrivée</b></label>
		<input type="text" placeholder="Arrivée" name="arrivee" id="arrivee" onfocus="hideArriveeValidite()" onblur="verificationArrivee()" required></input>
		<p id="arriveeValidite" class="rouge" for="arrivee" hidden></p>
		
		<label for="tarif"><b>Tarif par kilomètre</b></label>
		<input type="number" min="0" step="1" placeholder="Tarif par kilomètre" name="tarif" id="tarif" required></input>
		
		<label for="nbPlaces"><b>Nombre de places</b></label>
		<input type="number" min="1" placeholder="Nombre de places" name="nbPlaces" id="nbPlaces" required></input>
		
		<label for="heureDepart"><b>Heure de départ</b></label><br/>
		<input type="number" min="0" max="23" placeholder="Heure de départ" name="heureDepart" id="heureDepart" required></input>
		
		<label for="contraintes"><b>Contraintes</b></label><br/>
		<input type="text" maxlength="500" placeholder="Contraintes" name="contraintes" id="contraintes"></input>
		
		<div class="clearfix">
			<button type="submit" id="proposerVoyageBouton" class="submitBtn" onclick="return ajouterVoyage()">Proposer le voyage</button>
		</div>
	</form>
</div>

<p id="voyagePropose" hidden>Votre proposition de voyage a bien été prise en compte</p>