<p>Nom : <?php echo $context->utilisateur->nom;?></p>
<p>Prenom : <?php echo $context->utilisateur->prenom;?></p>
<p>Pseudo : <?php echo $context->utilisateur->identifiant;?></p>

<p>Mes voyages reservés</p>
<div class="tableContainer">
	<table class="w3-table-all">
		<thead>
			<tr class="w3-blue">
				<th>Depart</th>
				<th>Arrivee</th>
				<th>Nom</th>
				<th>Prenom</th>
				<th>Heure de départ</th>
				<th>Heure d'arrivée</th>
				<th>Durée</th>
				<th>Tarif</th>
				<th>Contraintes</th>
				<th>Annuler réservation</th>
			</tr>
		</thead>
		<tbody>
	<?php foreach($context->tableData as $data) { ?>
			<tr>
				<td><?php echo $data['depart'];?></td>
				<td><?php echo $data['arrivee'];?></td>
				<td><?php echo $data['nom'];?></td>
				<td><?php echo $data['prenom'];?></td>
				<td><?php echo $data['heuredepart'];?>h</td>
				<td><?php echo $data['heurearrivee'];?>h<?php echo $data['minutearrivee'];?></td>
				<td><?php echo $data['heureduree'];?>h<?php echo $data['minuteduree'];?></td>
				<td><?php echo $data['tarif'];?>€</td>
				<td><?php echo $data['contraintes'];?></td>
				<td><button onclick="annulerReservation(<?php echo $data['id'];?>)">Annuler réservation</button>
			</tr>
	<?php }?>
		</tbody>
	</table>
</div>