<p>Liste des voyages de <?php echo $context->depart ?> à <?php echo $context->arrivee ?></p>
<div class="tableContainer">
	<table class="w3-table-all">
		<thead>
			<tr class="w3-blue">
				<th>Nombre de correspondance(s)</th>
				<th>Heure de départ</th>
				<th>Heure d'arrivée</th>
				<th>Durée</th>
				<th>Tarif</th>
				<th>Réserver ce(s) voyage(s)</th>
			</tr>
		</thead>
		<tbody>
	<?php 	$numeroVoyage = 0;
			foreach($context->tableData as $data) {
				$tarifTotal = 0;
				$dureeTotaleMinutes = $data[sizeof($data)-1]['minutearrivee'];;
				$dureeTotaleHeures = $data[sizeof($data)-1]['heurearrivee'] - $data[0]['heuredepart'];
				if($dureeTotaleHeures < 0) {
					$dureeTotaleHeures += 24;
				}
				for($i = 0; $i < sizeof($data); $i++) {
					$tarifTotal += $data[$i]['tarif'];
				}?>
				<tr>
					<td><?php echo sizeof($data)-1;?>&emsp;&emsp;&emsp;<span class="voyageGlobal" onclick="afficherCorrespondances('voyage<?php echo $numeroVoyage;?>')"> <i class="fa fa-chevron-down"></i> Plus de détails</span></td>
					<td><?php echo $data[0]['heuredepart'];?>h</td>
					<td><?php echo $data[sizeof($data)-1]['heurearrivee'];?>h<?php echo $data[sizeof($data)-1]['minutearrivee'];?></td>
					<td><?php echo $dureeTotaleHeures;?>h<?php echo $dureeTotaleMinutes;?></td>
					<td><?php echo $tarifTotal;?>€</td>
					<td><button
					<?php if(!isset($_COOKIE["id"])) {?> title="Connectez-vous pour réserver ces voyage" disabled 
					<?php } else {?> onclick="reserver('<?php echo $context->ids_voyages[$numeroVoyage];?>')"<?php }?>>Réserver</button></td>
				</tr>
				<tr id="voyage<?php echo $numeroVoyage;?>" class="correspondacesContainer" hidden>
					<td class="noPadding" colspan="6">
						<table class="w3-table-all">
							<thead>
								<tr class="w3-grey">
									<th>Depart</th>
									<th>Arrivée</th>
									<th>Nom</th>
									<th>Prenom</th>
									<th>Heure de départ</th>
									<th>Heure d'arrivée</th>
									<th>Durée</th>
									<th>Tarif</th>
									<th>Nombre de places</th>
									<th>Contraintes</th>
								</tr>
							</thead>
							<tbody>
						<?php 	foreach($data as $ligne) {?>
									<tr>
										<td><?php echo $ligne['depart'];?></td>
										<td><?php echo $ligne['arrivee'];?></td>
										<td><?php echo $ligne['nom'];?></td>
										<td><?php echo $ligne['prenom'];?></td>
										<td><?php echo $ligne['heuredepart'];?>h</td>
										<td><?php echo $ligne['heurearrivee'];?>h<?php echo $ligne['minutearrivee'];?></td>
										<td><?php echo $ligne['heureduree'];?>h<?php echo $ligne['minuteduree'];?></td>
										<td><?php echo $ligne['tarif'];?>€</td>
										<td><?php echo $ligne['nbplace'];?></td>
										<td><?php echo $ligne['contraintes'];?></td>
									</tr>
						<?php	}?>
							</tbody>
						</table>
					</td>
				</tr>
		<?php	$numeroVoyage++;
			} ?>
		</tbody>
	</table>
</div>

<p id="reservationEffectuee" hidden>Votre réservation a bien été prise en compte</p>