<div class="form">	
	<form id="inscriptionForm" method="POST">
		<h1>Inscription</h1>
		<hr>

		<label for="nom"><b>Nom</b></label>
		<input type="text" placeholder="Nom" name="nom" id="nom" required></input>

		<label for="prenom"><b>Prénom</b></label>
		<input type="text" placeholder="Prénom" name="prenom" id="prenom" required></input>
		
		<label for="pseudo"><b>Pseudo</b></label>
		<input type="text" placeholder="Pseudo" name="pseudo" id="pseudo" onfocus="hidePseudoValidite()" onblur="verificationPseudo()" required></input>
		<p id="pseudoValidite" class="rouge" for="pseudo" hidden>Le pseudo est indisponnible</p>
		
		<label for="mdp"><b>Mot de passe</b></label>
		<input type="password" placeholder="Mot de passe" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,45}" name="mdp" id="mdp" required></input>
		
		<label for="dateNaiss"><b>Date de naissance</b></label><br/>
		<input type="date" placeholder="Date de naissance" name="dateNaiss" id="dateNaiss" onchange="verificationAge()" required></input><br/><br/>
		<p id="ageValidite" class="rouge" for="pseudo" hidden>Vous devez avoir 18 ans pour vous inscrire</p>
		
		<div class="clearfix">
			<button type="submit" id="inscriptionBouton" class="submitBtn" onclick="return inscription()">S'inscrire</button>
		</div>
	</form>
</div>

