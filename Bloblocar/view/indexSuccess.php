<div class="form">
	<form id="indexForm" method="GET">
		<input type="hidden" name="action" value="voyage"></input>
		<h1>Recherche voyage</h1>
		<hr>

		<label for="email"><b>Départ</b></label>
		<input type="text" placeholder="Départ" name="depart" id="depart" required></input>

		<label for="psw"><b>Arrivée</b></label>
		<input type="text" placeholder="Arrivée" name="arrivee" id="arrivee" required></input>
		
		<div class="clearfix">
			<button type="submit" class="submitBtn" onclick="return chercherVoyage()">Rechercher</button>
		</div>
	</form>
</div>