<div class="form">	
	<form id="connexionForm" method="POST">
		<h1>Connexion</h1>
		<p id="connexionValidation" class="rouge" for="pseudo" hidden>Identifiants incorrects</p>
		<hr>
		
		<label for="pseudo"><b>Pseudo</b></label>
		<input type="text" placeholder="Pseudo" name="pseudo" id="pseudo" required></input>
		
		<label for="mdp"><b>Mot de passe</b></label>
		<input type="password" placeholder="Mot de passe" name="mdp" id="mdp" required></input> <br/>
		
		<label>
			<input type="checkbox" name="remember" id="remember"> Se souvenir de moi</input>
		</label>
		
		<div class="clearfix">
			<button type="submit" id="connexionBouton" class="submitBtn" onclick="return connexion()">Se connecter</button>
		</div>
	</form>
</div>