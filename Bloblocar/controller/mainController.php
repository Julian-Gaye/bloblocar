<?php

class mainController
{

	public static function helloWorld($request, $context)
	{
		$context->mavariable="hello world";
		return context::SUCCESS;
	}

	public static function index($request, $context){
		
		return context::SUCCESS;
	}
	
	public static function superTest($request, $context) {
		
		if(!array_key_exists('param1', $request) or !array_key_exists('param2', $request))
			return context::ERROR;
		$context->mavariable = "j'ai compris ".$request['param1'].", super : ".$request['param2'];
		return context::SUCCESS;
	}
	
	public static function test($request, $context) {
		$trajet = trajetTable::getTrajet("Montpellier", "Bordeaux");
		$voyage = voyageTable::getVoyageByTrajet($trajet);
		$reservation = reservationTable::getReservationByVoyage($voyage[0]);
		$user = utilisateurTable::getUserById(7404);
		
		$context->mavariable = "<br/>".$trajet->depart."<br/>".
		$trajet->arrivee."<br/>".
		$trajet->distance."<br/>".
		$trajet->id."<br/>".
		$voyage[0]->conducteur->prenom." ".$voyage[0]->trajet->depart."<br/>".
		$voyage[1]->conducteur->prenom." ".$voyage[0]->trajet->depart."<br/>".
		$reservation[0]->voyage->conducteur->nom."<br/>".
		$reservation[1]->voyage->contraintes."<br/>".
		$user->identifiant;
		return context::SUCCESS;
	}
	
	//Ancienne fonction de recherche de voyage (sans les correspondances)
	// public static function voyage($request, $context) {
		// if(!array_key_exists('depart', $request) or !array_key_exists('arrivee', $request))
			// return context::ERROR;
		// $context->depart = $request['depart'];
		// $context->arrivee = $request['arrivee'];
		// $trajet = trajetTable::getTrajet($request['depart'], $request['arrivee']);
		// if($trajet == null)
			// return context::ERROR;
		// $voyage = voyageTable::getVoyageByTrajet($trajet);
		// if($voyage == null)
			// return context::ERROR;
		// $tableData = array();
		// if(isset($_COOKIE["id"]))
			// $utilisateur = utilisateurTable::getUserById($_COOKIE["id"]);
		// foreach($voyage as $data) {
			// if((!isset($utilisateur) or $utilisateur->identifiant != $data->conducteur->identifiant) //On n'affiche pas les voyages proposés par l'utilisateur
			// and $data->nbplace != 0 //On n'affiche pas les voyages sans place
			// and (!isset($utilisateur) or reservationTable::getIfReservationExistsByVoyageAndUser($data, $utilisateur) == false)) { //On n'affiche pas les voyages déjà reservés par l'utilisateur
				// $heureduree = floor($trajet->distance/60);
				// $minuteduree = floor(round(($trajet->distance/60 - floor($trajet->distance/60))*100)/100*60);
				// if((int)$minuteduree < 10)
					// $minuteduree = '0' . $minuteduree;
				// $heurearrivee = $data->heuredepart + floor($trajet->distance/60);
				// if((int)$heurearrivee >= 24)
					// $heurearrivee = $heurearrivee%24;
				// if((int)$heurearrivee < 10)
					// $heurearrivee = '0' . $heurearrivee;
				// $minutearrivee = floor(round(($trajet->distance/60 - floor($trajet->distance/60))*100)/100*60);
				// if((int)$minutearrivee < 10)
					// $minutearrivee = '0' . $minutearrivee;
				// $ligne = array(
					// "prenom" => $data->conducteur->prenom, 
					// "nom" => $data->conducteur->nom, 
					// "heuredepart" => $data->heuredepart,
					// "heureduree" => $heureduree,
					// "minuteduree" => $minuteduree,
					// "heurearrivee" => $heurearrivee,
					// "minutearrivee" => $minutearrivee,
					// "tarif" => $data->tarif,
					// "contraintes" => $data->contraintes,
					// "nbplace" => $data->nbplace,
					// "id" => $data->id,
				// );
				// array_push($tableData, $ligne);
			// }
		// }
		// if(sizeof($tableData) == 0)
			// return context::ERROR;
		// $context->tableData = $tableData;
		// return context::SUCCESS;
	// }
	
	public static function voyage($request, $context) {
		if(!array_key_exists('depart', $request) or !array_key_exists('arrivee', $request))
			return context::ERROR;
		$context->depart = $request['depart'];
		$context->arrivee = $request['arrivee'];
		$trajet = trajetTable::getTrajet($request['depart'], $request['arrivee']);
		if($trajet == null)
			return context::ERROR;
		$listeVoyage = voyageTable::getCorrespondancesByTrajet($trajet);
		if($listeVoyage == null)
			return context::ERROR;
		$tableData = array();
		$ids_voyages = array();
		if(isset($_COOKIE["id"]))
			$utilisateur = utilisateurTable::getUserById($_COOKIE["id"]);
		foreach($listeVoyage as $correspondaces) {
			$afficher = true;
			$voyages = array();
			foreach(explode(',', $correspondaces['ids_correspondance']) as $correspondace) {
				$data = voyageTable::getVoyageById($correspondace);
				if((isset($utilisateur) and $utilisateur->identifiant == $data->conducteur->identifiant) //Si un voyage du voyage complet a été proposé par l'utilisateur
				or (isset($utilisateur) and reservationTable::getIfReservationExistsByVoyageAndUser($data, $utilisateur) != false)) { //Si l'utilisateur a déjà reservé ce voyage
					$afficher = false;
					break; //On n'affiche pas la correspondance
				}
				$heureduree = floor($data->trajet->distance/60); //On arrondi à l'inférieur les heures
				$minuteduree = ($data->trajet->distance - $heureduree*60); //On trouve le nombre de minutes
				if((int)$minuteduree < 10) //On met le nombre de minutes sur 2 chiffres
					$minuteduree = '0' . $minuteduree;
				$heurearrivee = $data->heuredepart + floor($data->trajet->distance/60);
				if((int)$heurearrivee >= 24) //On met le nombre d'heures au format 24h
					$heurearrivee = $heurearrivee%24;
				if((int)$heurearrivee < 10) //On met le nombre d'heures sur 2 chiffres
					$heurearrivee = '0' . $heurearrivee;
				$minutearrivee = ($data->trajet->distance%60);
				if((int)$minutearrivee < 10) //On met le nombre de minutes sur 2 chiffres
					$minutearrivee = '0' . $minutearrivee;
				$ligne = array( //On met dans un tableau un voyage d'un voyage complet
					"depart" => $data->trajet->depart,
					"arrivee" => $data->trajet->arrivee,
					"prenom" => $data->conducteur->prenom,
					"nom" => $data->conducteur->nom,
					"heuredepart" => $data->heuredepart,
					"heureduree" => $heureduree,
					"minuteduree" => $minuteduree,
					"heurearrivee" => $heurearrivee,
					"minutearrivee" => $minutearrivee,
					"tarif" => $data->tarif,
					"contraintes" => $data->contraintes,
					"nbplace" => $data->nbplace,
					"id" => $data->id,
				);
				array_push($voyages, $ligne); //On rajoute ce voyage à la liste de tous les voyages du voyage complet
			}
			if($afficher == true) {
				array_push($tableData, $voyages); //On rajoute ce voyage complet à la liste des voyages complets (tableau à 2 dimensions)
				array_push($ids_voyages, $correspondaces['ids_correspondance']); //On renvoie la liste des id dans un tableau séparé (pour faire les réservations)
			}
		}
		
		if(sizeof($tableData) == 0) //Si aucun voyage n'a été trouvé on renvoie la vue error
			return context::ERROR;
		$context->ids_voyages = $ids_voyages;
		$context->tableData = $tableData;
		return context::SUCCESS;
	}
	
	public static function inscription($request, $context) {
		return context::SUCCESS; //On renvoie la bonne vue directement (pas d'erreur possible)
	}
	
	public static function verificationInscription($request, $context) {
		if(array_key_exists('pseudo', $request))
			echo utilisateurTable::pseudoIsUsed($request['pseudo']); //On vérifie que le pseudo n'est pas déjà pris
		return context::NONE;
	}
	
	public static function inscrireUtilisateur($request, $context) {
		if(array_key_exists('pseudo', $request) and array_key_exists('mdp', $request) and array_key_exists('nom', $request) and array_key_exists('prenom', $request))
			utilisateurTable::insertUser($request['pseudo'], $request['mdp'], $request['nom'], $request['prenom']); //On rajoute l'utilisateur à la table des utilisateurs
		return context::NONE;
	}
	
	public static function connexion($request, $context) {
		return context::SUCCESS; //On renvoie la bonne vue directement (pas d'erreur possible)
	}
	
	public static function connecterUtilisateur($request, $context) {
		if(array_key_exists('pseudo', $request) and array_key_exists('mdp', $request))
			echo utilisateurTable::getUserByLoginAndPass($request['pseudo'], $request['mdp']); //On connecte l'utilisateur si les identifiants sont corrects
		else
			echo false;
		return context::NONE;
	}
	
	public static function reserver($request, $context) {
		if(array_key_exists('idVoyages', $request) and isset($_COOKIE["id"])) {
			$utilisateur = utilisateurTable::getUserById($_COOKIE["id"]);
			foreach(explode(',', $request['idVoyages']) as $idVoyage) { //On sépare chaque id de la liste des id (séparé par des virugles)
				$voyage = voyageTable::getVoyageById($idVoyage);
				reservationTable::reserverVoyage($voyage, $utilisateur); //On réserve chaque voyage de la liste pour l'utilisateur connecté
			}
		}
		return context::NONE;
	}
	
	public static function informations($request, $context) {
		$context->utilisateur = utilisateurTable::getUserById($_COOKIE["id"]);
		$reseration = reservationTable::getReservationByUser($context->utilisateur);
		
		if($reseration == false)
			return context::ERROR;
		$tableData = array();
		foreach($reseration as $data) {
			$heureduree = floor($data->voyage->trajet->distance/60);
			$minuteduree = ($data->voyage->trajet->distance - $heureduree*60);
			if((int)$minuteduree < 10) 
				$minuteduree = '0' . $minuteduree;
			$heurearrivee = $data->voyage->heuredepart + floor($data->voyage->trajet->distance/60);
			if((int)$heurearrivee >= 24)
				$heurearrivee = $heurearrivee%24;
			if((int)$heurearrivee < 10)
				$heurearrivee = '0' . $heurearrivee;
			$minutearrivee = ($data->voyage->trajet->distance%60);
			if((int)$minutearrivee < 10)
				$minutearrivee = '0' . $minutearrivee;
			$ligne = array(
				"depart" => $data->voyage->trajet->depart,
				"arrivee" => $data->voyage->trajet->arrivee,
				"prenom" => $data->voyage->conducteur->prenom, 
                "nom" => $data->voyage->conducteur->nom, 
                "heuredepart" => $data->voyage->heuredepart,
                "heureduree" => $heureduree,
				"minuteduree" => $minuteduree,
				"heurearrivee" => $heurearrivee,
				"minutearrivee" => $minutearrivee,
                "tarif" => $data->voyage->tarif,
                "contraintes" => $data->voyage->contraintes,
                "nbplace" => $data->voyage->nbplace,
				"id" => $data->voyage->id
			);
			array_push($tableData, $ligne);
		}
		$context->tableData = $tableData;
		return context::SUCCESS;
	}
	
	public static function annulerReservation($request, $context) {
		if(array_key_exists('idVoyage', $request) and isset($_COOKIE["id"])) {
			$utilisateur = utilisateurTable::getUserById($_COOKIE["id"]);
			$voyage = voyageTable::getVoyageById($request['idVoyage']);
			reservationTable::annulerReservation($voyage, $utilisateur); //On annule la réservation correspondante pour l'utilisateur
		}
		return context::NONE;
	}
	
	public static function proposerVoyage($request, $context) {
		return context::SUCCESS; //On renvoie la bonne vue directement (pas d'erreur possible)
	}
	
	public static function verificationVille($request, $context) {
		if(array_key_exists('ville', $request))
			echo trajetTable::voyageExists($request['ville']); //On vérifie que la ville existe dans la base de données
		return context::NONE;
	}
	
	public static function ajouterVoyage($request, $context) {
		if(array_key_exists('depart', $request) and array_key_exists('arrivee', $request) and array_key_exists('tarif', $request) and array_key_exists('nbPlaces', $request) and array_key_exists('heureDepart', $request) and array_key_exists('contraintes', $request) and isset($_COOKIE["id"])) {
			$trajet = trajetTable::getTrajet($request['depart'], $request['arrivee']);
			$conducteur = utilisateurTable::getUserById($_COOKIE["id"]); //On récupère l'utilisateur connecté via le cookie de connexion
			voyageTable::ajouterVoyage($trajet, $conducteur, (int)$request['tarif']*$trajet->distance, $request['nbPlaces'], $request['heureDepart'], $request['contraintes']);  //On ajoute toutes les informations dans la base de données (pour créer un voyage)
		}
		return context::NONE;
	}
}

?>