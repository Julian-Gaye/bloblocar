<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css"/>
		<link rel="stylesheet" href="././css/style.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<title>
			Bloblocar
		</title>
	</head>
	
	<body>
		<?php if(isset($_COOKIE["id"])) {
			$utilisateur = utilisateurTable::getUserById($_COOKIE["id"]);
		}?>
		<header>
			
			<div class="w3-top">
				<!-- Navbar -->
				<div class="w3-bar w3-red w3-card w3-left-align w3-large">
					<a id="menuNavBtn" class="w3-bar-item w3-button w3-hide-large w3-left w3-hover-white w3-large w3-red" onclick="navigationMenu()"><i id="menuNavBtnIcone" class="fa fa-bars"></i></a>
					<a class="w3-bar-item w3-hide-small w3-hide-medium w3-button w3-hover-white w3-padding-large w3-left" onclick="action('index')">Chercher voyage</a>
					<?php
					if(isset($utilisateur)) {?>
						<a class="w3-bar-item w3-button w3-padding-large w3-right w3-hover-white" onclick="deconnexion()">Déconnexion</a>
						<a id="profilMenuBtn" class="w3-bar-item w3-button w3-right w3-padding-large w3-hover-white w3-large w3-red" onclick="profilMenu()">Profil</a>
						<div id="profilMenu" class="w3-bar-block w3-light-grey w3-hide w3-large">
							<a class="w3-bar-item w3-button w3-padding-large w3-right" onclick="action('informations')">Mes informations</a>
							<a class="w3-bar-item w3-button w3-padding-large w3-right" onclick="action('proposerVoyage')">Proposer un voyage</a>
						</div>
					<?php }
					else {?>
						<a class="w3-bar-item w3-button w3-padding-large w3-hover-white w3-right" onclick="action('connexion')">Connexion</a>
						<a class="w3-bar-item w3-button w3-padding-large w3-hover-white w3-right" onclick="action('inscription')">Inscription</a>
					<?php 
					}?>
				</div>
				<!-- Navbar on small screens -->
				<div id="navMenu" class="w3-bar-block w3-light-grey w3-hide w3-hide-large w3-large">
					<a class="w3-bar-item w3-hide-large w3-button w3-padding-large w3-left" onclick="action('index')">Chercher voyage</a>
				</div>
			</div>
		</header>
		<div id="contenu">
			<h2 id="messageBvn">Bienvenue sur Bloblocar !</h2>
			<?php if($context->error): ?>
				<div id="flash_error" class="error">
					<?php echo " $context->error !!!!!" ?>
				</div>
			<?php endif; ?>
			<div id="page_maincontent">
				<?php include($template_view);?>
			</div>
		</div>
	</body>
</html>
