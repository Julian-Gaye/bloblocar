create or replace function reserver() returns trigger as $$
declare
begin	
	if(TG_OP = 'INSERT') then
		update voyage set nbplace = nbplace - 1 where id = new.voyage;
	else
		update voyage set nbplace = nbplace + 1 where id = old.voyage;
	end if;
	return null;
end;
$$ LANGUAGE plpgsql;

--create trigger t_reserver after insert or delete on reservation for each row execute procedure reserver();