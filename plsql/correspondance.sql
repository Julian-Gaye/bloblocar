create or replace function find_voyage(dep trajet.depart%TYPE, arr trajet.arrivee%TYPE, heureArriveePrecedenteEnMinutes integer default 0, distanceParcourue trajet.distance%TYPE default 0, premiereFonction boolean default true) returns TABLE(ids_correspondance varchar(1000)) as $$
declare
var_arrivee trajet.arrivee%TYPE;
var_id_correspondance voyage.id%TYPE;
var_distanceTotale trajet.distance%TYPE;
var_tempsPause integer;
var_heureDepartEnMinutes integer;
var_heureArriveeEnMinutes integer;
curseur refcursor;
correspondance_record record;
begin
	for var_arrivee, var_id_correspondance, var_distanceTotale, var_heureDepartEnMinutes, var_heureArriveeEnMinutes in
		select trajet.arrivee, voyage.id, trajet.distance + distanceParcourue, voyage.heuredepart*60, voyage.heuredepart*60 + trajet.distance
		from voyage join trajet on voyage.trajet = trajet.id
		where depart = dep
		and nbplace > 0
	loop
		if(premiereFonction = false) then --On rajoute le temps de pause entre la fin du dernier voyage et le début du suivant dans le temps total de parcours
			if(var_heureDepartEnMinutes >= heureArriveePrecedenteEnMinutes) then
				var_tempsPause := var_heureDepartEnMinutes - heureArriveePrecedenteEnMinutes;
			else --Si le trajet à lieu le lendemain
				var_tempsPause := var_heureDepartEnMinutes - heureArriveePrecedenteEnMinutes + 1440;
			end if;
			var_distanceTotale := var_distanceTotale + var_tempsPause;
		end if;
		if(var_arrivee is not null and var_distanceTotale <= 1440) then --Si on trouve des voyages partant de la ville de départ selectionnée et que le trajet total ne dure pas plus de 24h
			if(var_arrivee = arr) then --Si on trouve un voyage qui correspond à la ville d'arrivée (fin de la récursivité)
				ids_correspondance := var_id_correspondance;
				return NEXT;
				
			else --Sinon on cherche d'autres trajets qui mènent à la ville d'arrivée (récursivité)
				open curseur for
					select *
					from find_voyage(var_arrivee, arr, var_heureArriveeEnMinutes, var_distanceTotale, false); --On appelle la fonction récursivement
				loop
					fetch curseur into correspondance_record;
					exit when not found;
					ids_correspondance := concat(var_id_correspondance, ',', correspondance_record.ids_correspondance);
					return NEXT; --On ajoute la correspondance actuelle aux correspondances déjà trouvées
				end loop;
				close curseur;
			end if;
		end if;
	end loop;
end;
$$ LANGUAGE plpgsql