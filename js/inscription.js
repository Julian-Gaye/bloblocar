function verificationAge() {
	var dateMin = new Date();
	var dateEntree = new Date();
	var jour = dateMin.getDate();
	var mois = dateMin.getMonth()+1;
	var annee = dateMin.getFullYear() - 18;
	if(jour < 10) {
	   jour = '0' + jour
	} 
	if(mois < 10) {
		mois = '0' + mois
	} 
	dateMin = annee + '-' + mois + '-' + jour;
	dateEntree = document.getElementById("dateNaiss").value
	if(dateMin < dateEntree) { //On bloque le bouton de validation et on informe l'utilisateur qu'il doit avoir 18 ans
		document.getElementById("ageValidite").hidden = false;
		document.getElementById("inscriptionBouton").disabled = true;
	}
	else {
		document.getElementById("ageValidite").hidden = true;
		if(document.getElementById("pseudoValidite").hidden == true) { //Si le pseudo est aussi valide
			document.getElementById("inscriptionBouton").disabled = false; //On réactive le bouton de validation
		}
	}
}

function hidePseudoValidite() {
	pseudoValidite = document.getElementById("pseudoValidite");
	if(document.getElementById("ageValidite").hidden == true) {
		bouton = document.getElementById("inscriptionBouton");
		bouton.disabled = false;
	}
	pseudoValidite.hidden = true;
}

function inscription() {
	var inscriptionForm = document.getElementById("inscriptionForm");
	if(inscriptionForm.checkValidity()) {
		var pseudo = document.getElementById("pseudo").value;
		var mdp = document.getElementById("mdp").value;
		var nom = document.getElementById("nom").value;
		var prenom = document.getElementById("prenom").value;
		inscriptionRequete(pseudo, mdp, nom, prenom);
		return false;
	}
}

function inscriptionRequete(pseudo, mdp, nom, prenom) {
	var btn = document.getElementsByClassName("submitBtn");
	btn[0].disabled = true; //On empêche l'utilisateur de faire plusieurs requêtes AJAX (en bloquant le bouton)
	btn[0].innerHTML = "<i class=\"fa fa-spinner fa-spin\"></i> Inscription en cours"; //Et on met un message sur le bouton
	if(window.ActiveXObject) {
		try {
			xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e) {
			xhr=new ActiveXObject("MSXML2.XMLHTTP");
		}
	}
	else if(window.XMLHttpRequest){
		xhr=new XMLHttpRequest();
	}
	xhr.onreadystatechange=recupereReponseInscription;
	urlAjax = "dispatcherAjax.php?action=inscrireUtilisateur&pseudo=" + pseudo + "&mdp=" + mdp + "&nom=" + nom + "&prenom=" + prenom;
	xhr.open("POST", urlAjax, true);
	xhr.send(null);
}

function recupereReponseInscription(){
	if((xhr.readyState==4) && (xhr.status==200)){
		envoiRequete("connexion"); //On affiche la page de connexion une fois l'inscription effectuée
	}
}

function verificationPseudo() {
	var pseudo = document.getElementById("pseudo").value;
	if(pseudo == "")
		return;
	verificationPseudoRequete(pseudo);
}

function verificationPseudoRequete(pseudo) {
	if(window.ActiveXObject) {
		try {
			xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e) {
			xhr=new ActiveXObject("MSXML2.XMLHTTP");
		}
	}
	else if(window.XMLHttpRequest){
		xhr=new XMLHttpRequest();
	}
	xhr.onreadystatechange=recupereReponseVerificationPseudo;
	urlAjax = "dispatcherAjax.php?action=verificationInscription&pseudo=" + pseudo;
	xhr.open("POST", urlAjax, true);
	xhr.send(null);
}

function recupereReponseVerificationPseudo(){
	if((xhr.readyState==4) && (xhr.status==200)){
		if(xhr.responseText == true) { //Si le pseudo existe déjà
			pseudoValidite = document.getElementById("pseudoValidite");
			bouton = document.getElementById("inscriptionBouton");
			bouton.disabled = true;
			pseudoValidite.hidden = false;
		}
	}
}