function reserver(idVoyages) {
	reserverRequete(idVoyages);
}

function reserverRequete(idVoyages) {
	if(window.ActiveXObject) {
		try {
			xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e) {
			xhr=new ActiveXObject("MSXML2.XMLHTTP");
		}
	}
	else if(window.XMLHttpRequest){
		xhr=new XMLHttpRequest();
	}
	xhr.onreadystatechange=recupereReponseReserver;
	urlAjax = "dispatcherAjax.php?action=reserver&idVoyages=" + idVoyages;
	xhr.open("POST", urlAjax, true);
	xhr.send(null);
}

function recupereReponseReserver() {
	if((xhr.readyState==4) && (xhr.status==200)) {
		url = new URL(window.location.href);
		envoiRequeteReservationEffectuee(url.searchParams.get("depart"), url.searchParams.get("arrivee")); //On recharge la page pour afficher les changements
	}
}

function envoiRequeteReservationEffectuee(depart, arrivee) {
	if(window.ActiveXObject) {
		try {
			xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e) {
			xhr=new ActiveXObject("MSXML2.XMLHTTP");
		}
	}
	else if(window.XMLHttpRequest){
		xhr=new XMLHttpRequest();
	}
	xhr.onreadystatechange=recupereReponseReservationEffectuee;
	urlAjax = "dispatcherAjax.php?action=voyage&depart=" + depart  + "&arrivee=" + arrivee;
	
	xhr.open("GET", urlAjax, true);
	xhr.send(null);
}

function recupereReponseReservationEffectuee() {
	if((xhr.readyState==4) && (xhr.status==200)) {
		document.getElementById("page_maincontent").innerHTML = xhr.responseText;
		document.getElementById("reservationEffectuee").hidden = false;
	}
}

function annulerReservation(id) {
	annulerReservationRequete(id);
}

function annulerReservationRequete(id) {
	if(window.ActiveXObject) {
		try {
			xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e) {
			xhr=new ActiveXObject("MSXML2.XMLHTTP");
		}
	}
	else if(window.XMLHttpRequest){
		xhr=new XMLHttpRequest();
	}
	xhr.onreadystatechange=recupereReponseAnnulerReservation;
	urlAjax = "dispatcherAjax.php?action=annulerReservation&idVoyage=" + id;
	
	xhr.open("POST", urlAjax, true);
	xhr.send(null);
}

function recupereReponseAnnulerReservation() {
	if((xhr.readyState==4) && (xhr.status==200)) {
		action("informations"); //On recharge la page pour afficher les changements
	}
}

function hideDepartValidite() {
	departValidite = document.getElementById("departValidite");
	if(document.getElementById("arriveeValidite").hidden == true) {
		bouton = document.getElementById("proposerVoyageBouton");
		bouton.disabled = false;
	}
	departValidite.hidden = true;
}

function verificationDepart() {
	var depart = document.getElementById("depart").value;
	if(depart == "")
		return;
	verificationDepartRequete(depart);
}

function verificationDepartRequete(depart) {
	if(window.ActiveXObject) {
		try {
			xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e) {
			xhr=new ActiveXObject("MSXML2.XMLHTTP");
		}
	}
	else if(window.XMLHttpRequest){
		xhr=new XMLHttpRequest();
	}
	xhr.onreadystatechange=recupereReponseVerificationDepart;
	urlAjax = "dispatcherAjax.php?action=verificationVille&ville=" + depart;
	xhr.open("POST", urlAjax, true);
	xhr.send(null);
}

function recupereReponseVerificationDepart(){
	if((xhr.readyState==4) && (xhr.status==200)){
		if(xhr.responseText == false) { //Si la ville n'existe pas
			departValidite = document.getElementById("departValidite");
			depart = document.getElementById("depart").value;
			bouton = document.getElementById("proposerVoyageBouton");
			departValidite.innerHTML = "La ville " + depart + " n'existe pas"; //On affiche une erreur
			bouton.disabled = true; //On bloque le bouton de proposition de voyage
			departValidite.hidden = false;
		}
	}
}

function hideArriveeValidite() {
	arriveeValidite = document.getElementById("arriveeValidite");
	if(document.getElementById("departValidite").hidden == true) {
		bouton = document.getElementById("proposerVoyageBouton");
		bouton.disabled = false;
	}
	arriveeValidite.hidden = true;
}

function verificationArrivee() {
	var arrivee = document.getElementById("arrivee").value;
	if(arrivee == "")
		return;
	verificationArriveeRequete(arrivee);
}

function verificationArriveeRequete(arrivee) {
	if(window.ActiveXObject) {
		try {
			xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e) {
			xhr=new ActiveXObject("MSXML2.XMLHTTP");
		}
	}
	else if(window.XMLHttpRequest){
		xhr=new XMLHttpRequest();
	}
	xhr.onreadystatechange=recupereReponseVerificationArrivee;
	urlAjax = "dispatcherAjax.php?action=verificationVille&ville=" + arrivee;
	xhr.open("POST", urlAjax, true);
	xhr.send(null);
}

function recupereReponseVerificationArrivee(){
	if((xhr.readyState==4) && (xhr.status==200)){
		if(xhr.responseText == false) { //Si la ville n'existe pas
			arriveeValidite = document.getElementById("arriveeValidite");
			arrivee = document.getElementById("arrivee").value;
			bouton = document.getElementById("proposerVoyageBouton");
			arriveeValidite.innerHTML = "La ville " + arrivee + " n'existe pas"; //On affiche une erreur
			bouton.disabled = true; //On désactive le bouton
			arriveeValidite.hidden = false;
		}
	}
}

function ajouterVoyage() {
	var proposerVoyageForm = document.getElementById("proposerVoyageForm");
	if(proposerVoyageForm.checkValidity()) {
		var depart = document.getElementById("depart").value;
		var arrivee = document.getElementById("arrivee").value;
		var tarif = document.getElementById("tarif").value;
		var nbPlaces = document.getElementById("nbPlaces").value;
		var heureDepart = document.getElementById("heureDepart").value;
		var contraintes = document.getElementById("contraintes").value;
		if(contraintes == "")
			contraintes = "Aucune"; //On met "Aucune" si l'input contraintes a été laissée vide
			ajouterVoyageRequete(depart, arrivee, tarif, nbPlaces, heureDepart, contraintes);
		return false;
	}
	
}

function ajouterVoyageRequete(depart, arrivee, tarif, nbPlaces, heureDepart, contraintes) {
	var btn = document.getElementsByClassName("submitBtn"); //On empêche l'utilisateur de faire plusieurs requêtes AJAX (en bloquant le bouton)
	btn[0].disabled = true;
	btn[0].innerHTML = "<i class=\"fa fa-spinner fa-spin\"></i> Proposition du voyage en cours";
	if(window.ActiveXObject) {
		try {
			xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e) {
			xhr=new ActiveXObject("MSXML2.XMLHTTP");
		}
	}
	else if(window.XMLHttpRequest){
		xhr=new XMLHttpRequest();
	}
	xhr.onreadystatechange=recupereReponseAjouterVoyage;
	urlAjax = "dispatcherAjax.php?action=ajouterVoyage&depart=" + depart + "&arrivee=" + arrivee + "&tarif=" + tarif + "&nbPlaces=" + nbPlaces + "&heureDepart=" + heureDepart + "&contraintes=" + contraintes;
	
	xhr.open("POST", urlAjax, true);
	xhr.send(null);
}

function recupereReponseAjouterVoyage() {
	if((xhr.readyState==4) && (xhr.status==200)) {
		document.getElementById("voyagePropose").hidden = false; //On informe l'utilisateur que son voyage a bien été pris en compte
		document.getElementById("proposerVoyageForm").reset(); //On supprime les valeurs entrées dans le formulaire
		var btn = document.getElementsByClassName("submitBtn")
		btn[0].disabled = false; //On réactive le bouton une fois la requête effectuée
		btn[0].innerHTML = "Proposer le voyage";
	}
}