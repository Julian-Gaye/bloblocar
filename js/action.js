function chercherVoyage() {
	var indexForm = document.getElementById("indexForm");
	if(indexForm.checkValidity()) {
		var depart = document.getElementById("depart").value;
		var arrivee = document.getElementById("arrivee").value;
		depart = depart.charAt(0).toUpperCase() + depart.slice(1).toLowerCase();
		arrivee = arrivee.charAt(0).toUpperCase() + arrivee.slice(1).toLowerCase();
		envoiRequeteVoyage(depart, arrivee);
		return false;
	}
}

function action(action) {
	envoiRequete(action);
	return false;
}

function envoiRequeteVoyage(depart, arrivee) {
	var btn = document.getElementsByClassName("submitBtn");
	btn[0].disabled = true;
	btn[0].innerHTML = "<i class=\"fa fa-spinner fa-spin\"></i> Recherche en cours";
	if(window.ActiveXObject) {
		try {
			xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e) {
			xhr=new ActiveXObject("MSXML2.XMLHTTP");
		}
	}
	else if(window.XMLHttpRequest){
		xhr=new XMLHttpRequest();
	}
	xhr.onreadystatechange=recupereReponse;
	urlAjax = "dispatcherAjax.php?action=voyage&depart=" + depart  + "&arrivee=" + arrivee;
	url = "Bloblocar.php?action=voyage&depart=" + depart  + "&arrivee=" + arrivee;
	xhr.open("GET", urlAjax, true);
	xhr.send(null);
}

function envoiRequete(action) {
	if(window.ActiveXObject) {
		try {
			xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e) {
			xhr=new ActiveXObject("MSXML2.XMLHTTP");
		}
	}
	else if(window.XMLHttpRequest){
		xhr=new XMLHttpRequest();
	}
	xhr.onreadystatechange=recupereReponse;
	urlAjax = "dispatcherAjax.php?action=" + action;
	url = "Bloblocar.php?action=" + action;
	xhr.open("GET", urlAjax, true);
	xhr.send(null);
}

function recupereReponse(){
	if((xhr.readyState==4) && (xhr.status==200)){
		document.getElementById("page_maincontent").innerHTML = xhr.responseText;
		window.history.pushState('', 'Bloblocar', url);
	}
}

function envoiRequeteRechargerPage() {
	if(window.ActiveXObject) {
		try {
			xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e) {
			xhr=new ActiveXObject("MSXML2.XMLHTTP");
		}
	}
	else if(window.XMLHttpRequest){
		xhr=new XMLHttpRequest();
	}
	xhr.onreadystatechange=recupereReponseRechargerPage;
	url = "Bloblocar.php?action=index";
	
	xhr.open("GET", url, true);
	xhr.send(null);
}

function recupereReponseRechargerPage() {
	if((xhr.readyState==4) && (xhr.status==200)){
		window.history.pushState('', 'Bloblocar', url);
		document.open();
        document.write(xhr.responseText);
        document.close();
	}
}
