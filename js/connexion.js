function connexion() {
	var connexionForm = document.getElementById("connexionForm");
	if(connexionForm.checkValidity()) {
		var pseudo = document.getElementById("pseudo").value;
		var mdp = document.getElementById("mdp").value;
		var remember = document.getElementById("remember").value;
		envoiRequeteConnexion(pseudo, mdp);
		return false;
	}
}

function envoiRequeteConnexion(pseudo, mdp) {
	var btn = document.getElementsByClassName("submitBtn"); //On empêche l'utilisateur de faire plusieurs requêtes AJAX (en bloquant le bouton)
	btn[0].disabled = true;
	btn[0].innerHTML = "<i class=\"fa fa-spinner fa-spin\"></i> Connexion en cours";
	if(window.ActiveXObject) {
		try {
			xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e) {
			xhr=new ActiveXObject("MSXML2.XMLHTTP");
		}
	}
	else if(window.XMLHttpRequest){
		xhr=new XMLHttpRequest();
	}
	xhr.onreadystatechange=recupereReponseConnexion;
	urlAjax = "dispatcherAjax.php?action=connecterUtilisateur&pseudo=" + pseudo  + "&mdp=" + mdp;
	
	xhr.open("POST", urlAjax, true);
	xhr.send(null);
}

function recupereReponseConnexion() {
	if((xhr.readyState==4) && (xhr.status==200)){
		var btn = document.getElementsByClassName("submitBtn"); //On réactive le bouton de validation de connexion
		btn[0].disabled = false;
		btn[0].innerHTML = "Se connecter";
		if(xhr.responseText == false) {
			document.getElementById("connexionValidation").hidden = false;
		}
		else {
			document.getElementById("connexionValidation").hidden = true;
			var d = new Date();
			if(remember.checked) {
				d.setTime(d.getTime() + (365*24*60*60*1000));
				document.cookie = "id=" + xhr.responseText + ";expires=" + d.toUTCString(); //On créer un cookie qui dure à l'infini (1000 ans)
			}
			else
				document.cookie = "id=" + xhr.responseText; //On créer un cookie qui sera détruit une fois le navigateur fermé
			envoiRequeteRechargerPage(); //On recharge toute la page pour connecter l'utilisateur
		}
	}
}