function afficherCorrespondances(idVoyage) { //Sert à afficher les détails de chaque voyage du voyage complet
	var voyage = document.getElementById(idVoyage)
	if(voyage.hidden == false) //Si les détails du voyage ont déjà été ouverts
		voyage.hidden = true; //On masque les détails
	else {
		var listVoyage = document.getElementsByClassName("correspondacesContainer");
		for (var i = 0; i < listVoyage.length; i++) {
			listVoyage[i].hidden = true; //On cache les détails éventuellement déjà ouverts
		}
		voyage.hidden = false; //On affiche les bons détails
	}
}