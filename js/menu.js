function navigationMenu() {
	var navMenu = document.getElementById("navMenu");
	if (navMenu.className.indexOf("w3-show") == -1) { //On ouvre le menu si il est fermé
		navMenu.className += " w3-show";
	} 
	else { //On ferme le menu si il est ouvert
		navMenu.className = navMenu.className.replace(" w3-show", "");
	}
}

function profilMenu() {
	var profilMenu = document.getElementById("profilMenu");
	if (profilMenu.className.indexOf("w3-show") == -1) { //On ouvre le menu si il est fermé
		profilMenu.className += " w3-show";
	} 
	else { //On ferme le menu si il est ouvert
		profilMenu.className = profilMenu.className.replace(" w3-show", "");
	}
}

function fermerMenus(event) { //Pour fermer les menus si on clique n'importe où sur l'écran
	var profilMenu = document.getElementById("profilMenu");
	var navMenu = document.getElementById("navMenu");
	if(event.target.id != "menuNavBtn" && event.target.id != "menuNavBtnIcone") {
		navMenu.className = navMenu.className.replace(" w3-show", "");
	}
	if(profilMenu != null && event.target.id != "profilMenuBtn") {
		profilMenu.className = profilMenu.className.replace(" w3-show", "");
	}
}

document.addEventListener('click', fermerMenus);
